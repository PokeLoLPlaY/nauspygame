import pygame
 
import random

FPS=60
WINDOW_SIZE_X=800
WINDOW_SIZE_Y=600
PLAYER_VEL=0.2
PLAYER_CoolDown=1000
METEOR_TIME=3000
POWERUP_TIME=8000
WAVE_TIME=60000

# game states
GAME_STATE_MENU=1
GAME_STATE_VOLUME=2
GAME_STATE_PLAYING=3
GAME_STATE_RESULTS=4
GAME_STATE_EXIT=5
GAME_STATE_PLAYING2=6
GAME_STATE_WIN=7


def load_sounds():
    bite_sounds=[]
    for n in range(1,2):
        name="sounds/bite"+str(n)+".ogg"
        bite_sounds.append(pygame.mixer.Sound(name))
    return bite_sounds

def playSound(sounds, ID):
    sound=sounds[ID]
    sound.play()

def setVolumens(sounds, volume):
    for sound in sounds:
        sound.set_volume(volume/100)
    return sounds


def create_meteor_types():
    meteor_types={'petit':{'sprite': pygame.image.load('Image/Meteors/meteorpetit.png'),
                        'next': None,
                        'vel':0.1,
                        'points':7,
                        'damage':9}
    }
    meteor_types['mitja']={'sprite': pygame.image.load('Image/Meteors/meteormitja.png'),
                        'next': meteor_types["petit"],
                        'vel': 0.075,
                        'points':5,
                        'damage':13}
    meteor_types['gran']={'sprite': pygame.image.load('Image/Meteors/meteorgran.png'),
                        'next': meteor_types["mitja"],
                        'vel': 0.05,
                        'points':3,
                        'damage':17}
    return meteor_types

def createexpImages():

    expImages=[]
    for n in range(0,7):
        expImages.append(pygame.image.load('Image/Explosion/tile00'+str(n)+'.png'))
    return expImages
    

def createExplosions(expImages):
    explosion={
        'x':0,
        'y':0,
        'sprites': expImages,
        'time':20
    }
    return explosion

def spawnnewboom(explosionobj, explosionList,x, y):
    newexp={
        'x':x,
        'y':y,
        'sprite':explosionobj["sprites"][0],
        'time':explosionobj["time"],
        'currentTime':0,
        'index':0
    }
    explosionList.append(newexp)



def spawn_meteor(meteor_types):

    meteor_type=None
    n=random.randint(0,3)
    if n==0:
        meteor_type=meteor_types["petit"]
    elif n==1:
        meteor_type=meteor_types["mitja"]
    else: 
        meteor_type=meteor_types["gran"]

    meteor={
        'sprite': meteor_type['sprite'],
        'next': meteor_type['next'],
        'x': random.randint(0, WINDOW_SIZE_X-meteor_type['sprite'].get_width()),
        'y': 0,
        'vel':meteor_type['vel'],
        'points':meteor_type['points'],
        'damage':meteor_type['damage'],
    }
    n=random.randint(0,2)
    meteor['dir']=n
    return meteor

def spawn_next_meteor(meteor):

    meteor={
        'sprite': meteor['next']['sprite'],
        'next': meteor['next']['next'],
        'x': meteor['x'],
        'y': meteor['y'],
        'vel':meteor['next']['vel'],
        'points':meteor['next']['points'],
        'damage':meteor['next']['damage'],
        'dir':meteor['dir']
    }
    return meteor

def move_meteors(meteors, delta, player, hearts, ID):
    loose=False
    for n in range(len(meteors)-1,-1,-1):
        meteor=meteors[n]
        meteor['y']+=meteor['vel']*delta

        if ID==1:
            if meteor['dir']==0:
                meteor['x']-=meteor['vel']*delta
                if meteor['x']<=0:
                    meteor['dir']=1
            else:
                meteor['x']+=meteor['vel']*delta
                if meteor['x']>=WINDOW_SIZE_X-60:
                    meteor['dir']=0


        if meteor['y']>=WINDOW_SIZE_Y:
            if(player['lifes']>0):
                player['lifes']-=1
                hearts.remove(hearts[len(hearts)-1])
                meteors.remove(meteor)
            else:
                loose=True 
    return loose


def draw_meteors(screen, meteors):
    for meteor in meteors:
        sprite=meteor['sprite']
        square=sprite.get_rect().move(meteor['x'], meteor['y'])
        screen.blit(sprite, square)

def draw_explisions(screen, explosionList, delta, expImages):
    exptodelete=[]
    for exp in explosionList:
        exp["currentTime"]+=delta
        if(exp["time"]<exp["currentTime"]):
            if(exp["index"]<len(expImages)-1):    
                exp["index"]+=1
                exp["currentTime"]=0
                exp["sprite"]=expImages[exp["index"]]
            else:
                exptodelete.append(exp)

        sprite=exp["sprite"]
        square=sprite.get_rect().move(exp['x'], exp['y'])
        screen.blit(sprite,square)
    for exp in exptodelete:

        explosionList.remove(exp)




def create_laser_sprites():

    laserSprites={
                    '1':pygame.image.load('Image/Projectile/laserBlue01.png')

                 }
    
    return laserSprites

def disparar(Player,sounds, shoots, laserSprites,listShoots):
    if Player['canshoot']==True:    
        spawnshoot(Player, laserSprites,listShoots)
        Player['canshoot']==False
        playSound(sounds, 0)



def spawnshoot(Player,laserSprites,ShootsList):

    if Player['shootID']==0:

        shoot={
                'sprite':laserSprites['1'],
                'vel':0.5,
                'x':Player['x']+45,
                'y':Player['y']-40,
                'dir':Player['dir']
            }
        
        if shoot['dir']=='up':
            shoot['sprite']=pygame.transform.rotate(laserSprites['1'],0)
            shoot['x']= Player['x']+45
            shoot['y']= Player['y']-40
            
        elif shoot['dir']=="right":
            shoot['sprite']=pygame.transform.rotate(laserSprites['1'],-45)
            shoot['x']= Player['x']+63.5
            shoot['y']= Player['y']-25

        elif shoot['dir']=='left':
            shoot['sprite']=pygame.transform.rotate(laserSprites['1'],45)
            shoot['x']= Player['x']-25
            shoot['y']= Player['y']-25

        ShootsList.append(shoot)
    
    elif Player['shootID']==1:
        newShoots=[]
        if Player['dir']=='up':
            shoot={
            'sprite':laserSprites['1'],
            'vel':0.5,
            'x':Player['x']+45,
            'y':Player['y']-40,
            'dir':'up'
            }
            newShoots.append(shoot)

            shoot2={
                'sprite':laserSprites['1'],
                'vel':0.5,
                'x':Player['x']+45,
                'y':Player['y']-40,
                'dir':'less-right'
            }
            shoot2['sprite']=pygame.transform.rotate(laserSprites['1'],-25)
            shoot2['x']= Player['x']+50
            shoot2['y']= Player['y']-25
            newShoots.append(shoot2)

            shoot3={
                'sprite':laserSprites['1'],
                'vel':0.5,
                'x':Player['x']+45,
                'y':Player['y']-40,
                'dir':'less-left'
            }
            shoot3['sprite']=pygame.transform.rotate(laserSprites['1'],25)
            shoot3['x']= Player['x']+17.5
            shoot3['y']= Player['y']-25
            newShoots.append(shoot3)

            for shoot in newShoots:
                ShootsList.append(shoot)
            
        elif Player['dir']=="right":
            shoot={
            'sprite':laserSprites['1'],
            'vel':0.5,
            'x':Player['x']+45,
            'y':Player['y']-40,
            'dir':'less-right'
            }
            shoot['sprite']=pygame.transform.rotate(laserSprites['1'],-25)
            newShoots.append(shoot)

            shoot2={
                'sprite':laserSprites['1'],
                'vel':0.5,
                'x':Player['x']+50,
                'y':Player['y']-40,
                'dir':'right'
            }
            shoot2['sprite']=pygame.transform.rotate(laserSprites['1'],-45)
            shoot2['x']= Player['x']+55
            shoot2['y']= Player['y']-25
            newShoots.append(shoot2)

            shoot3={
                'sprite':laserSprites['1'],
                'vel':0.5,
                'x':Player['x']+45,
                'y':Player['y']-40,
                'dir':'more-right'
            }
            shoot3['sprite']=pygame.transform.rotate(laserSprites['1'],-70)
            shoot3['x']= Player['x']+75
            shoot3['y']= Player['y']
            newShoots.append(shoot3)

            for shoot in newShoots:
                ShootsList.append(shoot)

        elif Player['dir']=='left':
            shoot={
            'sprite':laserSprites['1'],
            'vel':0.5,
            'x':Player['x']+45,
            'y':Player['y']-40,
            'dir':'less-left'
            }
            shoot['sprite']=pygame.transform.rotate(laserSprites['1'],25)
            shoot['x']= Player['x']
            shoot['y']= Player['y']-25
            newShoots.append(shoot)

            shoot2={
                'sprite':laserSprites['1'],
                'vel':0.5,
                'x':Player['x']+45,
                'y':Player['y']-40,
                'dir':'left'
            }
            shoot2['sprite']=pygame.transform.rotate(laserSprites['1'],45)
            shoot2['x']= Player['x']-25
            shoot2['y']= Player['y']-25
            newShoots.append(shoot2)

            shoot3={
                'sprite':laserSprites['1'],
                'vel':0.5,
                'x':Player['x']+45,
                'y':Player['y']-40,
                'dir':'more-left'
            }
            shoot3['sprite']=pygame.transform.rotate(laserSprites['1'],70)
            shoot3['x']= Player['x']-40
            shoot3['y']= Player['y']
            newShoots.append(shoot3)

            for shoot in newShoots:
                ShootsList.append(shoot)

        




def create_powerups_sprites():

    PowerUpsSprites={
                    '1':pygame.image.load('Image/PowerUps/bolt_bronze.png'),
                    '2':pygame.image.load('Image/PowerUps/bold_silver.png'),
                    '3':pygame.image.load('Image/PowerUps/bolt_gold.png')
                 }
    
    return PowerUpsSprites

def create_powerups_types(PowerUpsSprites):
    PowerUps={  
        'N1':{
            'ID':0,
            'sprite':PowerUpsSprites['1'],
            'ProjectileSpeed': 1,
            'shootRate':200,
            'time':10000
            },
        'N2':{
            'ID':1,
            'sprite':PowerUpsSprites['2'],
            'ProjectileSpeed': 0.5,
            'shootRate':700,
            'time':12000
        },
        'N3':{
            'ID':2,
            'sprite':PowerUpsSprites['3'],
            'ProjectileSpeed': 0.5,
            'shootRate':700, 
            'time':12000
        }
    }

    return PowerUps

def spawn_PowerUp(PowerUps):

    PowerUp_type=None
    n=random.randint(0,3)

    if n==0:
        PowerUp_type=PowerUps["N1"]

    elif n==1:
        PowerUp_type=PowerUps["N2"]

    else: 
        PowerUp_type=PowerUps["N3"]

    PowerUp={
        'sprite': PowerUp_type['sprite'],
        'x': random.randint(0, WINDOW_SIZE_X-PowerUp_type['sprite'].get_width()),
        'y': 0,
        'vel':0.1,
        'ID':PowerUp_type['ID'],
        'ProjectileSpeed':PowerUp_type['ProjectileSpeed'],
        'time':PowerUp_type['time'],
        'shootRate':PowerUp_type['shootRate']
    }

    return PowerUp

def move_powerups(powerups, delta):
    for n in range(len(powerups)-1,-1,-1):
        powerup=powerups[n]
        powerup['y']+=powerup['vel']*delta

def draw_powerups(screen, powerups):
    for powerup in powerups:
        sprite=powerup['sprite']
        square=sprite.get_rect().move(powerup['x'], powerup['y'])
        screen.blit(sprite, square)

def PowerUpsCollisionPlayer(player, powerUps, hearts):

    newPowerUplist=[]
    sprite=player['sprite']
    square=sprite.get_rect().move(player['x'], player['y'])

    for powerUp in powerUps:

        powerUp_square=powerUp['sprite'].get_rect().move(powerUp['x'], powerUp['y'])

        if square.colliderect(powerUp_square):
            newPowerUplist.append(powerUp)

            if(powerUp['ID']==2 and player['lifes']<6):
                hearts.append(createheart(hearts))
                player['lifes']+=1


            elif(powerUp['ID']!=2):
                player['shootID']=powerUp['ID']
                player['coolpowerup']=powerUp['time']
                player['shootfreq']=powerUp['shootRate']



    for powerUp in newPowerUplist:
        powerUps.remove(powerUp)

def MeteorCollisionPlayer(player, meteors,hearts):
    loose=False
    newMeteorlist=[]
    sprite=player['sprite']
    square=sprite.get_rect().move(player['x'], player['y'])
    for meteor in meteors:
        meteor_square=meteor['sprite'].get_rect().move(meteor['x'], meteor['y'])
        if square.colliderect(meteor_square):
            if(player['lifes']>0):
                player['lifes']-=1
                hearts.remove(hearts[len(hearts)-1])
                newMeteorlist.append(meteor)
            else:
                loose=True

    for meteor in newMeteorlist:
        meteors.remove(meteor)
    return loose


def MeteorCollisionShoots(Player, shoots, meteors, explosionList, explosion):

    newMeteorlist=[]
    newshootsList=[]
    for shoot in shoots:
        sprite=shoot['sprite']
        square=sprite.get_rect().move(shoot['x'], shoot['y'])
        for meteor in meteors:
            meteor_square=meteor['sprite'].get_rect().move(meteor['x'], meteor['y'])
            if square.colliderect(meteor_square):
                Player['points']+=meteor['points']
                newMeteorlist.append(meteor)   
                newshootsList.append(shoot)
                spawnnewboom(explosion, explosionList, meteor["x"],meteor["y"])    


    for meteor in newMeteorlist:
        if meteor in meteors:
            if meteor['next']!=None:
                meteors.append(spawn_next_meteor(meteor))
                meteors.remove(meteor)

            else:   
                meteors.remove(meteor)

    for shoot in newshootsList:
        shoots.remove(shoot) 

def draw_overlay(screen, font, player):
    font.render_to(screen, (50, 550), 'Puntuació: '+str(player['points']))

def draw_overlaypowerup(screen, font, player):
    font.render_to(screen, (700, 550), str(player['coolpowerup']))
    
def move_disparos(disparos, delta):
    for n in range(len(disparos)-1,-1,-1):
        disparo=disparos[n]

        if disparo['dir']=='up':
            disparo['y']-=disparo['vel']*delta

        elif disparo['dir']=='right':
            disparo['y']-=2*disparo['vel']*delta/3
            disparo['x']+=2*disparo['vel']*delta/3

        elif disparo['dir']=='left':
            disparo['y']-=2*disparo['vel']*delta/3
            disparo['x']-=2*disparo['vel']*delta/3
        elif disparo['dir']=='less-right':
            disparo['y']-=3*disparo['vel']*delta/3
            disparo['x']+=disparo['vel']*delta/3
        elif disparo['dir']=='more-right':
            disparo['y']-=disparo['vel']*delta/3
            disparo['x']+=3*disparo['vel']*delta/3
        elif disparo['dir']=='less-left':
            disparo['y']-=3*disparo['vel']*delta/3
            disparo['x']-=disparo['vel']*delta/3
        elif disparo['dir']=='more-left':
            disparo['y']-=disparo['vel']*delta/3
            disparo['x']-=3*disparo['vel']*delta/3


def draw_disparos(screen, disparos):
    for disparo in disparos:
        sprite=disparo['sprite']
        square=sprite.get_rect().move(disparo['x'], disparo['y'])
        screen.blit(sprite, square)

def move_player(player, delta,shoots,sounds, laserSprites,listShoots):
    moved=False
    vel=int(PLAYER_VEL*delta)
    keys = pygame.key.get_pressed()
    if keys[pygame.K_w]:
        if keys[pygame.K_a]:
            player['y']=min(player['y']-2*vel/3, WINDOW_SIZE_Y-80)
            player['x']=max(player['x']-2*vel/3, 0)
            player["sprite"] = player['left']
            player["dir"]="left"

        elif keys[pygame.K_d]:
            player['y']=min(player['y']-2*vel/3, WINDOW_SIZE_Y-80)
            player['x']=min(player['x']+2*vel/3, WINDOW_SIZE_X-99)
            player["dir"]="right"
            player["sprite"] = player['right']
        else:
            player['y']=min(player['y']-vel, WINDOW_SIZE_Y-80)
            player["sprite"] = player['up']
            player["dir"]="up"

    elif keys[pygame.K_s]:
        if keys[pygame.K_a]:
            player['y']=min(player['y']+2*vel/3, WINDOW_SIZE_Y-80)
            player['x']=max(player['x']-2*vel/3, 0)
            player["sprite"] = player['right']
            player["dir"]="right"

        elif keys[pygame.K_d]:
            player['y']=min(player['y']+2*vel/3, WINDOW_SIZE_Y-80)
            player['x']=min(player['x']+2*vel/3, WINDOW_SIZE_X-99)
            player["dir"]="left"
            player["sprite"] = player['left']
        else:
            player['y']=min(player['y']+vel, WINDOW_SIZE_Y-80)
            player["sprite"] = player['up'] 
            player["dir"]="up"

    elif keys[pygame.K_a]:
        player['x']=max(player['x']-vel, 0)
        player["dir"]="up"
        player["sprite"] = player['up'] 

    elif keys[pygame.K_d]:
        player['x']=min(player['x']+vel, WINDOW_SIZE_X-99)
        player["dir"]="up"
        player["sprite"] = player['up'] 

    else:
        player["sprite"] = player['up']
        player["dir"]="up"
    
    if keys[pygame.K_SPACE]: 
        #print("Entro")
        if player['canshoot']==True:
            disparar(player,sounds,shoots,laserSprites,listShoots)
            player['canshoot'] = False 
            #print(player['canshoot'])
            
    
    if player['canshoot']==False:
        player['cooldown']+=delta

        if player['cooldown']>=player['shootfreq']:
            player['canshoot']=True
            player['cooldown']=0

def createhearts(Player):
    
    hearts=[]
    for x in range(Player['lifes']):
        heart={
            'sprite':pygame.image.load('Image/heart/Hearts.png'),
            'ID':x,
            'x':25+x*50,
            'y':25
        }
        hearts.append(heart)

    return hearts

def createheart(hearts):
    x=len(hearts)
    heart={
            'sprite':pygame.image.load('Image/heart/Hearts.png'),
            'ID':x,
            'x':25+x*50,
            'y':25
        }
    return heart

def draw_Hearts(screen, hearts):
    for heart in hearts:
        sprite=heart['sprite']
        square=sprite.get_rect().move(heart['x'], heart['y'])
        screen.blit(sprite, square)

def draw_player(screen, player):
    sprite=player['sprite']
    square=sprite.get_rect().move(player['x'], player['y'])
    screen.blit(sprite, square)

def create_player():
    Player={
    'right': pygame.image.load('Image/player/PlayerShipRight.png'),
    'left': pygame.image.load('Image/player/PlayerShipLeft.png'),
    'up': pygame.image.load('Image/player/PlayerShip.png'),
    'sprite_index': 0,
    'x':400-49.5,
    'y':600-80,
    'dir':'Up',
    'canshoot':True,
    'shootfreq':PLAYER_CoolDown,
    'cooldown':0,
    'points':0,
    'shootID':0,
    'coolpowerup':0,
    'lifes':3
    }
    Player["sprite"] = Player['up']
    return Player


def game_menu(screen):
    title=pygame.image.load('Image/menu/title.png')
    start_btn_light=pygame.image.load('Image/menu/start_button.png')
    start_btn_dark=pygame.image.load('Image/menu/start_buttondark.png')
    exit_btn_light=pygame.image.load('Image/menu/exit_button.png')
    exit_btn_dark=pygame.image.load('Image/menu/exit_buttondark.png')
    volume_btn_light=pygame.image.load('Image/menu/volume_btn_light.png')
    volume_btn_dark=pygame.image.load('Image/menu/volume_btn_dark.png')
    start_btn=start_btn_light
    exit_btn=exit_btn_light
    volume_btn=volume_btn_light
    background=pygame.image.load('Image/menu/background.jpg')
    going=True
    while going:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                going=False
                result=GAME_STATE_EXIT
            if event.type == pygame.MOUSEBUTTONDOWN:
                square=start_btn.get_rect().move(150, 400)
                if square.collidepoint(pygame.mouse.get_pos()):
                    going=False
                    result=GAME_STATE_PLAYING
                square=exit_btn.get_rect().move(450, 400)
                if square.collidepoint(pygame.mouse.get_pos()):
                    going=False
                    result=GAME_STATE_EXIT
                square=volume_btn.get_rect().move(300, 500)
                if square.collidepoint(pygame.mouse.get_pos()):
                    going=False
                    result=GAME_STATE_VOLUME
                    
        square=start_btn.get_rect().move(150, 400)
        if square.collidepoint(pygame.mouse.get_pos()):
            start_btn=start_btn_dark
        else:
            start_btn=start_btn_light

        square=exit_btn.get_rect().move(450, 400)
        if square.collidepoint(pygame.mouse.get_pos()):
            exit_btn=exit_btn_dark
        else:
            exit_btn=exit_btn_light

        square=volume_btn.get_rect().move(300, 500)
        if square.collidepoint(pygame.mouse.get_pos()):
            volume_btn=volume_btn_dark
        else:
            volume_btn=volume_btn_light

        screen.blit(background, background.get_rect())
        screen.blit(title, title.get_rect().move(400-352, 50))
        screen.blit(start_btn, start_btn.get_rect().move(150,400))
        screen.blit(exit_btn, exit_btn.get_rect().move(450,400))
        screen.blit(volume_btn,volume_btn.get_rect().move(300,500))

        pygame.display.flip()
    return result        

def game_volume(screen,volume):
    font = pygame.freetype.Font("fonts/Lato-Black.ttf", 32)
    title=pygame.image.load('Image/menu/title.png')
    plus_btn_light=pygame.image.load('Image/menu/plus_btn_light.png')
    plus_btn_dark=pygame.image.load('Image/menu/plus_btn_dark.png')
    less_btn_light=pygame.image.load('Image/menu/less_btn_light.png')
    less_btn_dark=pygame.image.load('Image/menu/less_btn_dark.png')
    exit_btn_light=pygame.image.load('Image/menu/exit_button.png')
    exit_btn_dark=pygame.image.load('Image/menu/exit_buttondark.png')

    exit_btn=exit_btn_light
    plus_btn=plus_btn_light
    less_btn=less_btn_light

    background=pygame.image.load('Image/menu/newbackground.png')
    going=True
    while going:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                going=False
                result=GAME_STATE_EXIT
            if event.type == pygame.MOUSEBUTTONDOWN:
                square=plus_btn.get_rect().move(150, 400)
                if square.collidepoint(pygame.mouse.get_pos()) and volume<100:
                    volume+=5
                square=less_btn.get_rect().move(450, 400)
                if square.collidepoint(pygame.mouse.get_pos()) and volume>0:
                    volume-=5
                square=exit_btn.get_rect().move(300, 550)
                if square.collidepoint(pygame.mouse.get_pos()):
                    going=False
                    result=volume
                    
                    
        square=plus_btn.get_rect().move(150, 400)
        if square.collidepoint(pygame.mouse.get_pos()):
            plus_btn=plus_btn_dark
        else:
            plus_btn=plus_btn_light

        square=less_btn.get_rect().move(450, 400)
        if square.collidepoint(pygame.mouse.get_pos()):
            less_btn=less_btn_dark
        else:
            less_btn=less_btn_light

        square=exit_btn.get_rect().move(300, 550)
        if square.collidepoint(pygame.mouse.get_pos()):
            exit_btn=exit_btn_dark
        else:
            exit_btn=exit_btn_light

        screen.blit(background, background.get_rect())
        screen.blit(title, title.get_rect().move(400-352, 50))
        screen.blit(plus_btn, plus_btn.get_rect().move(150,400))
        screen.blit(less_btn, less_btn.get_rect().move(450,400))
        screen.blit(exit_btn, exit_btn.get_rect().move(300,550))
        font.render_to(screen, (300, 200), 'Volume: '+str(volume),)
        pygame.display.flip()
    return result        

def game_win(screen):
    font = pygame.freetype.Font("fonts/Lato-Black.ttf", 32)
    exit_btn_light=pygame.image.load('Image/menu/exit_button.png')
    exit_btn_dark=pygame.image.load('Image/menu/exit_buttondark.png')

    exit_btn=exit_btn_light

    background=pygame.image.load('Image/menu/newbackground.png')
    going=True
    while going:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                going=False
                result=GAME_STATE_EXIT
            if event.type == pygame.MOUSEBUTTONDOWN:
                square=exit_btn.get_rect().move(300, 550)
                if square.collidepoint(pygame.mouse.get_pos()):
                    going=False
                    result=GAME_STATE_MENU
                    
                    

        square=exit_btn.get_rect().move(300, 550)
        if square.collidepoint(pygame.mouse.get_pos()):
            exit_btn=exit_btn_dark
        else:
            exit_btn=exit_btn_light

        screen.blit(background, background.get_rect())
        screen.blit(exit_btn, exit_btn.get_rect().move(300,550))
        font.render_to(screen, (300, 200), 'YOU WIN ')
        pygame.display.flip()
    return result   

def game_playing(screen,ID,Player, volume):
    
    overlay_font = pygame.freetype.Font("fonts/Lato-Black.ttf", 32)
    if Player==None:
        Player=create_player()

    meteor_types=create_meteor_types()
    powerupsSprites=create_powerups_sprites()
    powerups_types=create_powerups_types(powerupsSprites)
    hearts=createhearts(Player)
    sounds=load_sounds()
    sounds=setVolumens(sounds, volume)
    #print(meteor_types)
    meteors=[]
    shoots=[]
    powerups=[]
    explosionList=[]
    laserSprites=create_laser_sprites()
    expImages=createexpImages()
    explosion=createExplosions(expImages)
    clock = pygame.time.Clock()
    elapsed_time_meteor=0
    elapsed_time_powerup=0
    elapsed_wave_time=0
    loose=False
    going=True
    endwave=False
    while going:
        delta=clock.tick(60)
        elapsed_time_meteor+=delta
        elapsed_time_powerup+=delta
        elapsed_wave_time+=delta
        if Player['coolpowerup']>0:
            Player['coolpowerup']-=delta

        #print(len(shoots))
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                going=False
                result=GAME_STATE_EXIT
        
        screen.fill((128,128,128))

        if elapsed_time_meteor>METEOR_TIME:
            elapsed_time_meteor-=METEOR_TIME
            meteors.append(spawn_meteor(meteor_types))
        
        if elapsed_time_powerup>POWERUP_TIME:
            elapsed_time_powerup-=POWERUP_TIME
            powerups.append(spawn_PowerUp(powerups_types))

        if elapsed_wave_time>WAVE_TIME:
            elapsed_wave_time-=0
            endwave=True
        
        
        if Player['coolpowerup']<0:
            Player['coolpowerup']=0
            Player['shootID']=0
            Player['shootfreq']=PLAYER_CoolDown

        move_player(Player, delta,shoots,sounds,laserSprites,shoots)
        loose=MeteorCollisionPlayer(Player, meteors, hearts)
        MeteorCollisionShoots(Player, shoots, meteors,explosionList,explosion)
        PowerUpsCollisionPlayer(Player, powerups, hearts)
        newloose=move_meteors(meteors, delta, Player, hearts, ID)
        if(loose==False and newloose):
            loose=True


        move_disparos(shoots, delta)
        move_powerups(powerups,delta)

        draw_meteors(screen, meteors)
        draw_player(screen, Player)
        draw_disparos(screen, shoots)
        draw_powerups(screen,powerups)
        draw_Hearts(screen,hearts)
        draw_overlay(screen, overlay_font,Player)
        draw_overlaypowerup(screen, overlay_font,Player)
        draw_explisions(screen,explosionList, delta ,expImages)
        pygame.display.flip()
        if(loose):
            going=False
            if ID==0:
                result=(GAME_STATE_MENU, None)

            else:
                result=GAME_STATE_MENU

        elif endwave:

            going=False
            if ID==0:
                result=(GAME_STATE_PLAYING2, Player)

            else:
                result=GAME_STATE_WIN
            
    return result

def main():
    pygame.init()

    screen = pygame.display.set_mode([800, 600])
    game_icon = pygame.image.load('Image/player/PlayerShip.png') 

    pygame.display.set_caption('Prova')
    pygame.display.set_icon(game_icon)

    player=None

    volume=100

    game_state=GAME_STATE_MENU
    
    while game_state!=GAME_STATE_EXIT:
        if game_state==GAME_STATE_MENU:
            game_state=game_menu(screen)
        elif game_state==GAME_STATE_VOLUME:
            volume=game_volume(screen,volume)
            game_state=GAME_STATE_MENU

        elif game_state==GAME_STATE_PLAYING:
           
            tupla=game_playing(screen,0,None,volume)
            for el in tupla:
                if isinstance(el, int) :
                    game_state=el
                else:
                    player=el

        elif game_state==GAME_STATE_PLAYING2:
            game_state=game_playing(screen,1,player,volume)

        elif game_state==GAME_STATE_WIN:
            game_state=game_win(screen)
    
    pygame.quit()

main()