# NausPyGame

Objectiu:

    Aguantar amb vida sense que els meterits acabin amb la terra que protegeixes (que els meteorits no sortin de la patalla)


Nau:

	Moviment: Moviment en eix X i eix Y
	Pot Disparar un projectil
	Pot rotar una mica i així apuntar als meteorits

Pojectil:

	Moviment: Moviment em eix X i eix y
	Quan fa col·lisió amb un meteorit es destrueix i player guanya punts

	--Differents tipus d'armes

        Basic (El jugador comença amb aquesta arma): Un projectil basic en llínea recta que canvia la seva direcció segons on miri el jugador 
        Multishoot(PowerUp): Tres projectils amb tres direccions differents.

Bonificadors:

	-Donarà altres projectils al jugador
	-Donarà més frequència a l'arma del jugador
	-Donarà una vida al jugador

Meteorits:

	-Tres tipus amb differents velocitats
	-Cadascú és més petit que l'anterior
	-Quan un meteorit és destruit, spawneja un altra més petit.
	-Quan fa col·lisió amb el player li treurà punts
	
	-Un cop entras a la segona fase els meteorits es mouen al eix X també


